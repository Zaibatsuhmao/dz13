int square(int a, int b)
{
	int aSqr = a * a;
	int bSqr = b * b;
	int c = 2 * a * b;
	int sum = aSqr + c + bSqr;
	return sum;
}